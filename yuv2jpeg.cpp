#include "globals.h"
#include "jpeg_encoder.h"

static const char* FILENAME     = "frameYUV420Large.raw";

static const char* OUT_FILENAME = "frame.jpg";

static const int NUM_CONVERSIONS = 1;

static const int FRAME_WIDTH = 640;
static const int FRAME_HEIGHT = 480;

static const int NANOS_PER_SEC = 1000000000;

// This should be inside the WIN32 ifdef
double get_time()
{
    timespec tp;
    clock_gettime(CLOCK_REALTIME, &tp);
    // ASSERT_THROW_ALWAYS(res==0, "error: {}", std::strerror(res));
    return double(tp.tv_sec) + (double(tp.tv_nsec) / NANOS_PER_SEC);
}

int main() {
    openlog("yuv2jpeg", LOG_CONS | LOG_NDELAY | LOG_PERROR | LOG_PID, LOG_USER);

    if (!jpegEncoder::init(FRAME_WIDTH, FRAME_HEIGHT)) {
        return EXIT_FAILURE;
    }

    unsigned char *imgBuf = NULL;
    unsigned char *jpegBuf = NULL;
    size_t len = 0;
    size_t jpegLen = 0;
    FILE *fp = NULL;

    fp = fopen(FILENAME, "rb");
    if (!fp) {
        syslog(LOG_ERR, "Could not open file.");
        return EXIT_FAILURE;
    }

    fseek(fp, 0, SEEK_END);
    len = jpegLen = ftell(fp);
    rewind(fp);
    imgBuf = (unsigned char *)malloc(len);
    jpegBuf = (unsigned char *)malloc(len);
    fread(imgBuf, 1, len, fp);
    fclose(fp);

    double start_time = get_time();
    
    size_t i=0;
    for(; i<NUM_CONVERSIONS; i++)
    {
        // printf("i=%d, ", i);
        if (jpegEncoder::encode(imgBuf, len, jpegBuf, &jpegLen) && jpegLen > 0) {
            // syslog(LOG_NOTICE, "Success!");
        } else {
                syslog(LOG_ERR, "Could not encode frame.");
                return EXIT_FAILURE;
        }
    }
    
    double end_time = get_time();
    
    syslog(LOG_NOTICE, "ran %d conversions, fps = %f", i, (end_time - start_time) / i);
    
    fp = fopen(OUT_FILENAME, "wb");
    if (fp) {
        fwrite(jpegBuf, 1, jpegLen, fp);
        fflush(fp);
        fclose(fp);
        syslog(LOG_NOTICE, "Success writing file %s\n", OUT_FILENAME);
    } else {
	syslog(LOG_ERR, "Failed to write file %s", OUT_FILENAME);
    }

    return EXIT_SUCCESS;
}

