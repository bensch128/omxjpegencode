*** Introduction ***

omxjpegencode originally was written by backz and 
posted at https://www.raspberrypi.org/forums/viewtopic.php?f=33&t=120694

dividuum made a patch to jpeg_encoder.cpp which made this functional.
This is merely a repackaging the code so it can be used as a library in 
anticipation of making a gstreamer plugin for fast encoding jpegs on RPi.

-------------------------------
*** How to build ***

clone this git into /opt/vc/src/hello_pi on your Raspberry Pi.

** Standable executable **
Run "make -f Makefile.bin" to build the yuv2jpeg.bin executable.
Run "yuv2jpeg.bin frameYUV420.raw" to get frame.jpeg to demonstrate that it works

** Shared Library **
Run "make -f Makefile.lib" to build libomxjpegencoder.so


Sincerely,
Ben

