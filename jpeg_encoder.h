#pragma once

#include <cstddef>

namespace jpegEncoder {
    bool init(size_t width, size_t height, bool do_logging=false);
    bool encode(const unsigned char *bufIn, size_t bufLenIn, unsigned char *bufOut, size_t *bufLenOut);
    void close();
}

